import React,{useState} from "react";
function Cart()
{
    let[products,setProducts]=useState(
        [
          {
            sno:1,
            image:'https://www.ruchifoodline.com/recipes//cdn/recipes/Best-Mutton-Biryani-Recipe.jpg',
            name:"Biryani",
            price:300,
            qty:2
          },  
          {
            sno:2,
            image:'https://www.licious.in/blog/wp-content/uploads/2022/02/shutterstock_761402230-600x600.jpg',
            name:"Kabab",
            price:200,
            qty:2
          },
          {
            sno:3,
            image:'https://img.freepik.com/premium-photo/light-cold-beer-frosty-glass-isolated-white_185709-12.jpg?w=2000',
            name:"Beer",
            price:190,
            qty:2
          },
        ]
      )
    return(
        <div>
           <div className="container-fluid">
                <div className="row">
                    <div className="col">
                        <h1 className="text-danger">Zomato Cart:-</h1>
                        <p className="lead">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quas delectus quos officiis cum est laudantium laboriosam incidunt doloribus facilis provident minus quisquam aliquid animi, repudiandae dicta nostrum dignissimos vero, dolore facere doloremque ratione nulla? Fugiat odio quae perferendis dicta et saepe, laborum ipsum quam aliquid assumenda molestias. Quos, ex cum obcaecati saepe perferendis accusantium culpa distinctio consequuntur. Ad, neque tempora accusamus nesciunt facere sunt! Ut maiores est eum similique labore modi saepe</p>
                        <button className="btn btn-danger">Order Now</button>
                    </div>
                </div>
                <section className="mt-5">
                    <div className="row">
                        <div className="col">
                            <table className="table table-hover table-light">
                                <thead className="text-danger text-center">
                                    <tr>
                                        <th>S.NO</th>
                                        <th>IMAGE</th>
                                        <th>NAME</th>
                                        <th>PRICE</th>
                                        <th>QTY</th>
                                        <th>TOTAL PRICE</th>
                                    </tr>
                                </thead>
                                <tbody className="text-center">
                                    {
                                        products.map((product)=>
                                        {
                                            return(
                                                <tr>
                                                    <td>{product.sno}</td>
                                                    <td><img src={product.image} width={30}/></td>
                                                    <td>{product.name}</td>
                                                    <td>{product.price}</td>
                                                    <td>{product.qty}</td>
                                                    <td>{product.price * product.qty}</td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </section>
           </div>
        </div>
    )
}
export default Cart;